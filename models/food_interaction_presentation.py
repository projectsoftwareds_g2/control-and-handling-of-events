from metamodels.guidance_metamodel import decision, design_solution, add_decision_option_link, single_answer, practice
from codeable_models import CClass, add_links, CBundle

# Create a top-level decision related to Interaction and Presentation in the Food industry
food_interaction_presentation_decision = CClass(
    decision, "Design Interaction and Presentation in the Food Industry", stereotype_instances=single_answer)

menu_display = CClass(design_solution, "Menu Display Design")
order_processing = CClass(design_solution, "Order Processing Interface Design")

add_decision_option_link(food_interaction_presentation_decision, menu_display, "Menu Display")
add_decision_option_link(food_interaction_presentation_decision, order_processing, "Order Processing Interface")

add_links({menu_display: food_interaction_presentation_decision}, role_name="next decision")
add_links({order_processing: food_interaction_presentation_decision}, role_name="next decision")

menu_display_decision = CClass(
    decision, "Menu Display Design Decisions", stereotype_instances=single_answer)
digital_menu = CClass(practice, "Digital Menu Board")
paper_menu = CClass(practice, "Paper Menu")

add_decision_option_link(menu_display_decision, digital_menu, "Digital Menu Display")
add_decision_option_link(menu_display_decision, paper_menu, "Paper Menu Design")

add_links({menu_display: menu_display_decision}, role_name="next decision")

order_processing_decision = CClass(
    decision, "Order Processing Interface Design Decisions", stereotype_instances=single_answer)
kiosk_system = CClass(practice, "Self-Service Kiosk System")
mobile_app = CClass(practice, "Mobile App Ordering")

add_decision_option_link(order_processing_decision, kiosk_system, "Self-Service Kiosk Design")
add_decision_option_link(order_processing_decision, mobile_app, "Mobile App Interface Design")

add_links({order_processing: order_processing_decision}, role_name="next decision")

food_interaction_presentation_bundle = CBundle("Interaction and Presentation in the Food Industry", elements=food_interaction_presentation_decision.class_object.get_connected_elements())

model_views = [food_interaction_presentation_bundle, {}]
